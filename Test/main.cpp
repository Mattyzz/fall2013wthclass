/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 24, 2013, 3:28 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv)
{
    cout << "Enter a number to make your half pyramind!";
    int x;
    cin >> x;
    for(int i = 0; i < x; i++)
    {
        cout << "*";
        for (int j = 0; j < i; j++)
        {
            cout << "*";
        }
        cout << endl;    
    }

    return 0;
}

