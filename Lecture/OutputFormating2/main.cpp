#include <iostream>
#include <iomanip>
using namespace std;
int main()
{
    cout << setw(10) << "Hello" << 3; //10spaces then print hello
    cout << endl << left << setw(10) << 17.123 << "H"; // setw effects 17.123
    return 0;
}

