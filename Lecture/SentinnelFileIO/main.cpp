/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on October 1, 2013, 3:32 PM
 */

#include <cstdlib>
#include <iostream>
#include <fstream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) 
{
    ifstream infile;
    infile.open("data.dat");
    
    if (!infile)
    {
        cout << "Invalid File" << endl;
    }
     string word; 
    while(infile >> word)
    {
       
        cout << word << endl;
    }

    return 0;
}

