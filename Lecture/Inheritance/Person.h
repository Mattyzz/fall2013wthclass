#include <cstdlib>
#include <iostream>
#include <string>

#include "Date.h"


using namespace std;


       



class Person
{
      private:
      string Name;
      Date DOB;
      string Description;
      
      public:
             void SetName(string name);
             string GetName();
             void SetDOB(int d);
             int GetDOB();
};

void Person::SetName(string name)
{
     Name = name;
}

string Person::GetName()
{
       return Name;
}

void Person::SetDOB(int d)
{
     DOB.set_month(d);
}

int Person::GetDOB()
{
    return DOB.get_month();
}
