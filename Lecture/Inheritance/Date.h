#include <cstdlib>
#include <iostream>
#include <string>



using namespace std;

class Date
{
private:
        int month;
        bool check_month(int m);
public:
       //Constructor
       Date(int);
       Date(); //Default
     
       
       //Member functions (access/mutate)
       int get_month();
       void set_month(int m);
       
       //Output function
       void output();
};


//Default Constructor
Date::Date() //Scope operator details the Date Function for the Date Class
{
 month = 0;             
}   

//Intialization selection
Date::Date(int m) // Or Date::Date(int m) : month(m)
{
    month = m;       
} 

//Getter
int Date::get_month()
{
      return month;
} 

//Setter
void Date::set_month(int m)
{
     if (check_month(m))
     {
        month = m;
     }
     else
     {
         cout << "Invalid month, set to 0/n";
         month = 0;
     }
}

bool Date::check_month(int m)
{
     //Check if valid month
     return m >= 1 && m <= 12;
}

void Date::output()
{
     cout << "Month is: " << month << endl;
}
        
