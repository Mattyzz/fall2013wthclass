/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on October 3, 2013, 2:50 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*Describe what the function does
 * do_something() gets a value and outputs that value
 * 
 * Describe parameters and their purpose
 * no Parameters
 * 
 * Describe the return value
 */

void do_something()
{
    int x;
    cout << "Enter a number " << endl;
    cin >> x;
    cout << "Entered " << x << endl;
}


void add5(int num)
    

int main(int argc, char** argv)
{
    do_something();
    add5(5);
    
    /*Describe the function
     * adds 5 to a value and prints it
     * 
     * Describe parameters
     * num is an integer that represents data provided by user
     * 
     * return value
     * returns nothing
     */
    

        int tot = num + 5;
        cout << tot << endl;
        add5(num);
        return;


    return ;
}

