#include <iostream>
#include <iomanip>
using namespace std;
int main()
{
    double num = 17.123;
    cout << setw(10) << "Hello" << 3; //10spaces then print hello
    cout << endl << left << setw(10) << num << "H"; // setw effects 17.123
    cout << endl << right << fixed << setprecision(2) << setw(10)
            << num << "H";
    cout << setfill('c') << endl << right 
            << fixed << setprecision(2) << setw(10) //fills white space with c
            << num << "H";
    return 0;
}
