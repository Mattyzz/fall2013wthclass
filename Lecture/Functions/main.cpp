/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 26, 2013, 3:26 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;


//Function Declaration
int sum(int,int);
//Function definition
double max(double left, double right)
{
    double high = 0;
    if(left > right)
        high = left;
    else
        high = right;
    return high;
}

int main(int argc, char** argv)
{
    int x =3;
    int y = 10;
    int tot = sum(x,y);
    cout << "Sum = " << tot << endl;
    cout << "Max = " << max(x,y) << endl;
    cout << "Max = " << max(x,21) << endl;
    
    return 0;
}

int sum(int num1, int num2)
{
    int total = num1 + num2;
    return total;
}
