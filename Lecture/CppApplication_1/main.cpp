/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 19, 2013, 3:45 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) 
{
    int x;
    do
    {
        
        cout << "Enter a number: ";
        cout << "Enter -1 to quit." << endl;
        cin >> x;
    }
    while (x != -1);
    return 0;
}

