#include <iostream>
#include <iomanip>
using namespace std;
int main()
{
    double num = 17.123;
    cout << setw(10) << "Hello" << 3; //10spaces then print hello
    cout << endl << left << setw(10) << num << "H"; // setw effects 17.123
    cout << endl << right << fixed << setprecision(2) << setw(10)
            << num << "H";
    return 0;
}
/*
 setw(10) only affects one variable
 setprecision(2) only allows 2 decimals and works with fixed
 using just 17.123 is know as hard-coding and leads to alot of errors
 */


