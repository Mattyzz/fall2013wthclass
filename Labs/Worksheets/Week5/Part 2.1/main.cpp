/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on October 10, 2013, 2:22 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;


/*
 * 
 */
int main(int argc, char** argv)
{
        int n;
        cin >> n;
        double x = 0;
        double s;
        while (s > 0.01)
        {
	s = 1.0 / (1 + n * n);
	n++;
	x = x + s;
        cout << x << endl;
        }
        


    return 0;
}

