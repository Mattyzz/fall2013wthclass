/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on October 1, 2013, 5:06 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) 
{
   char exit;
    do
    {
    string word;
    string reverse = "";
    bool same = true;
    cout << "I check if a word is a palindrome." << endl;
    cout << "Please enter a word, so I may check it.\n";
    cin >> word;
    
    int wordsize = word.size();
        for(int i = wordsize; i > 0; i--)
        {
                reverse += word[i - 1];
        
        }
                if(word == reverse)
                {
        
                cout << word << " is a palindrome.\n";
                 cout << endl;
                }       
                else
                {
                cout << word << " is not a palindrome\n";
                 }
    cout << "Try again? Y/N : ";
    cin >> exit;
    cout << endl;

    }
    while(exit == 'Y' || exit == 'y');
    
    return 0;
}

