/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 3, 2013, 4:32 PM
 */

#include <iostream>
using namespace std;
int main()
{
    int sum;
    int a,b,c,d,e,f,g,h;
    cout << "Please put in a number less than 100000.\n";
    cin >> sum;
    a = sum/10000; // divides get the first number
    b = sum%10000; // modulus operators get the remainder
    c = b/1000; // repeat
    d = b%1000;
    e = d/100;
    f = d%100;
    g = f/10;
    h = f%10;
            
    cout << a << " "
            << b << " " << endl
            << c << " "
            << d << " " << endl
            << e << " "
            << f << " " << endl
            << g << " "
            << h << " " << endl;
            
    return 0;
            
    
    
   
    
}
