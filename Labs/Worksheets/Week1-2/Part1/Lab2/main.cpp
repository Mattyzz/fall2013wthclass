#include <iostream>
using namespace std;
int main()
{
    cout << "Hello my name is Hal!\n"
            << "What is your name?\n";
    string name;
    cin >> name;
    cout << "Hello, " << name << ". "
            << "I am glad to meet you.\n";
    return 0;
    
}
