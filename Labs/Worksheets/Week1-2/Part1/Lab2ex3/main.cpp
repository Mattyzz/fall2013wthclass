/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 3, 2013, 4:32 PM
 */

#include <iostream>
using namespace std;
int main()
{
    int sum;
    int a,b,c,d,e,f,g,h;
    cout << "Please put in a number less than 100000.\n";
    cin >> sum;
    a = sum/10000; // divides get the first number 1
    b = sum%10000; // modulus operators get the remainder 0000
    c = b/1000; // repeat 0
    d = b%1000; // 000
    e = d/100; // 0
    f = d%100; // 00
    g = f/10; // 0
    h = f%10; // 0
            
    cout << a << " "
            << c << " "
            << e << " "
            << g << " "
            << h << " " << endl;
    cout << "Goodbye! :)";
    return 0;
            
    
    
   
    
}

