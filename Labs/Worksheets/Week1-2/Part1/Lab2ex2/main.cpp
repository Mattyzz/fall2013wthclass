/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 3, 2013, 4:22 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
int a = 5;
int b = 10;
int c = 6; // placeholder
cout << "a: "  << a << " " << "b: " << b << endl;
c = a;
//a=5 b=10 c=5
a = b; // b is the new a
// a=10 b=10 c=6
b = c;

cout << "a: " << a << " " << "b: " << b << endl;

    return 0;
}

