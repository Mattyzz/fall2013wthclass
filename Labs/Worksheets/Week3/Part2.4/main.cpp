/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 19, 2013, 3:15 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>


using namespace std;

/*
 * 
 */
int main(int argc, char** argv)
{
    string x;
    cout << "Please enter in a letter grade, EX: A+ or B- : ";
    cin >> x;
    if (x == "A+")
    {
        cout << "You got 100+%";
    }
    else if (x == "A")
    {
        cout << "You got 93-97.9%";
    }
    else if (x == "A-")
    {
        cout << "you got 90-92.9";
    }
    else if ( x == "B+")
    {
        cout << "You got 87-89.9%";
    }
    else if ( x == "B")
    {
        cout << "You got 86.9-83%";
    }
    else if ( x == "B-")
    {
        cout << "You got 80-82.9%";
                
    }
    else
    {
        cout << "You did not enter a valid letter grade";
                
    }
    
    return 0;
}

