/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 19, 2013, 2:32 PM
 */

#include <cstdlib>
#include <iostream>


using namespace std;

/*
 * 
 */
int main(int argc, char** argv)
{
    double given, price, gp;
    int d, q , ds, p, l, m, n, o, dollars, quarters, dimes, pennies, multiply;
    d = 100;
    q = 25;
    ds = 10;
    p = 1;
    cout << "Welcome to the checkout stand, please enter the cost: ";
    cin >> price;
    cout << "Now enter the amount you are going to pay:";
    cin >> given;
    
    if(given>price)
    {
        cout << "Now calculating please wait...";
        gp = given - price;
        multiply = gp * 100;
        dollars = multiply / d;
        l = multiply % d;
        quarters = l / q;
        m = l % q;
        dimes = m / ds;
        n = m % ds;
        pennies = n / p;
        
        cout << "Your change is: " << gp << "$" << endl;
        cout << "Dollars: " << dollars << endl
                << "Quarters: " << quarters << endl
                << "Dimes: " << dimes << endl
                << "Pennies: " << pennies << endl;
        cout << "Thank for checking out and not stealing from us!" << endl
                << "Goodbye!\n";
        
        
        
    }
    else
    {
        cout << "You did not give me enough money!\n"
                << "Please try again";
    }
            
          

    return 0;
}

