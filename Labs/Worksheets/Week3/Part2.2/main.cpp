/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 19, 2013, 2:53 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv)
{
    int x;
    cout << "Welcome to the grade caluclator!\n"
            << "Please enter a number between 1-100: ";
    cin >> x;
    if (x > 90)
    {
        cout << "You got a A!";
    }
    else if (x > 79)
    {
        cout << "You got a B!";   
    }
    else if (x > 69)
    {
        cout << "You got a C";
    }
    else if (x > 59)
    {
        cout << "You got a D...";
    }
    else if (x < 50)
    {
        cout << "You got a F, :(";
    }
    else
    {
        cout << "You did not enter a number between 1-100";
    }
    return 0;
}

