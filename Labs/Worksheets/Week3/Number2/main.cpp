#include <iostream>
#include <iomanip>
using namespace std;
int main()
{
    int x, y, s;
    string dash = "-";
    cout << "Welcome to number swapper!\n"
            <<"Please enter in x and then y\n";
    cin >> x >> y;
    if (x < 1000 && y < 1000) // if both numbers are less than 1000
    {
        cout <<  left <<  "x = " << setw(3)  << x; // prints out x = number
                cout << left << " y = "<< setw(3) << y << endl;         
        cout << setfill ('-') << setw(70) << dash << endl;
        cout << setfill(' ')  << "x = " << setw(3) << y;
        cout << " y = " << setw(3)<< x << endl;
                 
    }
    else // if numbers are greater than 1000
    {
        cout << "One of the numbers were greater than 1000,"
                <<" Please rerun the program and try again";
                
    }
    return 0;
    
    


   
}