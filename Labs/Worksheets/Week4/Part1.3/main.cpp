/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 24, 2013, 4:20 PM
 */
/*	A year with 366 days is called a leap year. 
 * A year is a leap year if it is divisible by 4 (for example, 1980),
 *  except that it is not a leap year if it is divisible by 100 
 * (for example, 1900).
 *  However, a leap year is divisible by 400, (for example, 2000).
 *  Write a program that asks the user for a year and computes whether that 
 * year is a leap year. 
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) 
{
    
    int years;
    cout << "Please enter a year: ";
    cin  >> years;


    if (0 == years % 4 && 0 == years % 400 )
    {
        cout << "Year " << years << " is a leap year.";
    }
    else if ( 0 ==  years % 100 )
    {
        cout << "Year " << years << " is not a leap year";
    }
    else if ( 0 == years % 4)
    {
        cout << "Year " << years << " is a leap year";
    }
    else
    {
        cout << "Year " << years << " is not a leap year";
    }


    
                
    

    return 0;
}

