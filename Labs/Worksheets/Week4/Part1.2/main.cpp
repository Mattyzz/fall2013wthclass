/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 19, 2013, 4:50 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv)

{
    string x;
    do
    {
    int n;
    double r, guess, guesshold, check;
    cout << "I find the square root of numbers!\n";
    cout << "Please enter a number: ";
    cin >> n;
    guess = n / 2;
    
    do
    {
        r = n / guess;
        guesshold = guess;
        guess = (guess + r)/ 2.0;
        check = guesshold - guess;
        
    }
    while (check > guesshold * .01);
    
    cout << "The square root of " << n << " is "
            << guess << endl;
    cout << "Please type stop to end, if not enter something else.\n";
    cin >> x;
    }
    while ( x != "stop");

    


    return 0;
}

