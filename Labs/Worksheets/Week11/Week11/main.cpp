/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on November 5, 2013, 4:42 PM
 */

#include <cstdlib>
#include <iostream>
#include <vector>
#include <fstream>

using namespace std;

void Menu();
void Problem1(); 
void Problem2(); 
void Problem3(); 
void Problem5(); 
void Problem6();
void OutputV(vector<int> & V);
void OutputA(int a[], int size);


int main(int argc, char** argv) 
{
    srand(time(0));
    Menu();

    return 0;
}
void Menu()
{
    int choice;
    cout << "Please put a number 1, 2, 3, 5, or 6. Any else to exit!\n";
    cin >> choice;
    
    switch (choice)
    {
            case 1:
                Problem1();
                break;
            case 2:
                Problem2();
                break;
            case 3:
                Problem3();
                break;
            case 5:
                Problem5();
                break;
            case 6:
                Problem6();
            default:
                cout << "Now leaving the program!\n";
    }
           
}

void Problem1()
{
    const int SIZE = 10;
    vector<int> V;
    int A[SIZE];
    
    cout << "Adding random numbers between 1-100 to Array...\n";
    cout << "Adding random numbers between 1-100 to Vectors..\n";
    
    for (int i = 0; i < SIZE; i++)
    {
        V.push_back(rand() % 100);
    }
    for (int i = 0; i < SIZE; i++)
    {
        A[i] = rand() % 100;
    }
    
    cout << "Vector numbers: \n";
    OutputV(V);


    
    cout << "\n";
    cout << "Array numbers: \n";
    OutputA(A, SIZE);
}

void Problem2()
{
    cout << "Problem 2 Stub\n";
}

void Problem3()
{
    cout << "Problem 3 Stub\n";
}

void Problem5()
{
cout << "Problem 5 Stub\n";
}

void Problem6()
{
cout << "Problem 6 Stub\n";
}


void OutputV(vector<int> & V)
{
    for(int i = 0; i < V.size(); i++)
    {
        cout << "Element " << i << " is " << V[i] << endl;
    }
            

}

void OutputA(int a[], int size)
{
    for(int i = 0; i < size; i++)
    {
        cout << "Element " << i << " is " << a[i] << endl;
    }
}