/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on October 10, 2013, 4:58 PM
 */

#include <cstdlib>
#include <iostream>


using namespace std;

/*
 *Def: Square root function takes a number to a specified power
 *
 * Parameters:  A value num represents the number you want to take to a power
 * while the value exp is the power
 * 
 * Return: the number the represents the num to the exp power
 */
double squareroot(double num, int exp = 1);
int squareroot(int num, int exp = 1);




/*
 * 
 */
int main(int argc, char** argv) 
{
    double a;
    int b;
    double ans;
    
    cout << "Enter a number " << endl;
    cin >> a;
    cout << "Enter a power, enter a character if you do not want one." << endl;
    cin >> b;
    ans = squareroot(a,b);
    cout << ans<< endl;

    return 0;
}
double squareroot(double num, int exp)
{
    int numholder = num;
    for(int i = 1; i < exp; i++)
    {    
        numholder = numholder * num;
    }
    return numholder;
}
int squareroot(int num, int exp )
{
    int numholder = num;
    for(int i = 1; i < exp; i++)
    {    
        numholder = numholder * num;
    }
    return numholder;
}



