/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on October 8, 2013, 4:23 PM
 */

#include <cstdlib>
#include <iostream>
#include <fstream>

using namespace std;

void filecounter()
{
    int total = 0;
    ifstream infile;
    infile.open("data.dat");
    string word;
    while(infile >> word)
    {
        total = total + word.size();
    }
    cout << "The total number of characters is " << total << endl;
    
}
/*
 * 
 */
int main(int argc, char** argv) 
{
    cout << "I count the ammount of words in a data file\n";
    filecounter();

    return 0;
}

