/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on October 29, 2013, 4:08 PM
 */

#include <cstdlib>
#include <vector>
#include <iostream>

using namespace std;

void Menu();
void Problem1();
void Problem2();
void Problem3();
void Problem4();
void VectorOutput(vector<int> & v);



int main(int argc, char** argv)
{
    Menu();
    return 0;
}
void Menu()
{
    int choice;
    
    cout << "Please enter a number 1-4 for the corresponding problem.\n";
    cout << "Enter any other number to close the program!\n";
    cin >> choice;
    
            
    switch (choice)
    {
        case 1:
            Problem1();
            break;
        case 2:
            Problem2();
            break;
        case 3:
            Problem3();
            break;
        case 4:
            Problem4();
            break;
        default:
            cout << "Now exiting...\n";

            
    }

}
void Problem1()
{
    int menureturn;
    vector<int> numberholder;
    
    numberholder.push_back(3);
    numberholder.push_back(6);
    numberholder.push_back(8);
    cout << "You have " << numberholder.size() << " numbers in this vector\n";
    
    VectorOutput(numberholder);
    
    cout << endl; 
    cout << "Please type 1 to return main menu, any else to close program\n";
    cin >> menureturn;
    
    if (menureturn == 1)
    {
        Menu();
    }
    else
    {
        cout << "Closing WK8 PT2 Worksheet Goodbye!\n";
    }
    
}
void Problem2()
{
    vector<int> nums;
    int number;
    int menureturn;
    
    
    cout << "Please enter as many numbers as you wish!\n";
    cout << "Enter -1 to end number input and output your numbers\n";
    
    do
    {        
        cin >> number;
        nums.push_back(number);
    }
    while(number != -1);
    VectorOutput(nums);
    
    cout << endl;
    cout << "Please type 1 to return main menu, any else to close program\n";
    cin >> menureturn;
    if (menureturn == 1)
    {
        Menu();
    }
    else
    {
        cout << "Closing WK8 PT2 Worksheet Goodbye!\n";
    }
    

}
void Problem3()
{
    
    int menureturn;
    
    vector<int> driver;
    driver.push_back(1);
    driver.push_back(7);
    driver.push_back(11);
    driver.push_back(22);
    driver.push_back(6);
    driver.push_back(9);
    
    cout << "Running the VectorOutput Function!\n";
    VectorOutput(driver);
    
        cout << endl;
    cout << "Please type 1 to return main menu, any else to close program\n";
    cin >> menureturn;
    if (menureturn == 1)
    {
        Menu();
    }
    else
    {
        cout << "Closing WK8 PT2 Worksheet Goodbye!\n";
    }
    
            
}
void Problem4()
{
    vector<int> nums;
    int number;
    int menureturn;
    
    
    cout << "Please enter as many numbers as you wish!\n";
    cout << "This program output before and after you input a number.\n";
    cout << "Enter -1 to end number input\n";
    
    
    do
    {      
        cout << "Current numbers: " << endl;
        VectorOutput(nums);
                
        cin >> number;
        nums.push_back(number);
        cout << endl;
        
        cout << "New numbers: " << endl;
        VectorOutput(nums);
    }
    while(number != -1);
 
    
    cout << endl;
    cout << "Please type 1 to return main menu, any else to close program\n";
    cin >> menureturn;
    if (menureturn == 1)
    {
        Menu();
    }
    else
    {
        cout << "Closing WK8 PT2 Worksheet Goodbye!\n";
    }
    

}
void VectorOutput(vector<int> & v)
{
    for (int i = 0; i < v.size(); i++)
    {
        cout << "Number " << i << " is: " << v[i] << endl;
    }
}




