
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <cctype>

using namespace std;

void Problem1();
void Problem2();
void Problem3();
void Problem4();
void Problem5();
void Menu();
void fix_file(ofstream & outfile); //Helper for Problem 4
int main(int argc, char** argv) 
{
    Menu();
}
void Problem1()
{
    string sentences;
    string userinput;
    cout << "Please enter sentences to your pleasure!\n"
            << "Enter the @ symbol when you are done\n";
    
    
    
    
    while(userinput != "@") //get rid of the @ sign
    {
        
        cin >> userinput;
        if(userinput == "@")
        {
          sentences = sentences;  
        }
        else
        {
        sentences = sentences  + userinput + " ";
        }
    }
    

    
    
    ofstream outfile;
    outfile.open("output.dat");
    outfile <<  sentences << endl;
    outfile.close();
    
    cout << "Saved you sentences to output.dat!\n";
    cout << "Now returning to main menu!\n";
    cout << endl;

    return Menu();
}
void Problem2()
{
    char userinput;
    cout << "Please enter your sentences!\n";

    ofstream outfile;
    outfile.open("output2.dat");
    do
    {

        cin.get(userinput); // do not use in outfile (Puts out memory address)
        //cout << "Userinput " << userinput << endl;
        if(userinput == '@')
        {
            cout << "Stopped files\n";
        }
        else
        {
        outfile << userinput;
        }
    }
    while (userinput != '@');
    

    
    


    outfile.close();
    
    cout << "Saved you sentences to output2.dat!\n";
    cout << "Now returning to main menu!\n";
    cout << endl;

    return Menu();
}
void Problem3()
{
    cout << "cin.get() gets the individual characters of a userinput\n";
    cout << "cin >> gets a full word or line untill it hits whitespace\n";
    cout << "getline(cin, string) appends the string\n";
    cout << endl;
    return Menu();
}
void Problem4()
{
    ofstream outfile;
    cout << "What is the name of the file you want to open?\n";
    string filename;
    cin >> filename;
    
    outfile.open(filename, ios::app);
    
    if(outfile.fail())
    {
        cout << "Failed\n";
        fix_file(outfile);
                
    }
    else
    {
        cout << "Found file!, now printing its contents\n";
        cout << outfile << endl;
    }
    
            
}
void Problem5()
{
    ofstream outfile;
    outfile.open("Problem5.dat");
    cout << "Current is :" << outfile << endl;
    
    outfile = tolower(outfile);
    
    cout << "Newest is :" << outfile << endl;
    
    outfile.close();
    
}
void Menu()
{
    int menuchoice;
    cout << "Welcome to Lab 8 Part 1 Worksheet!\n";
    cout << "Please select what problem you would like to view!\n";
    cout << "Enter a number between 1 and 5 for a problem\n";
    cout << "Enter any other number to end program!\n";
    cin >> menuchoice;
    switch (menuchoice)
    {
        case 1:
            Problem1();
            break;
        case 2:
            Problem2();
            break;
            
        case 3:
            Problem3();
            break;
            
        case 4:
            Problem4();
            break;
            
        case 5:
            Problem5();
        default:
            cout << "Now leaving Lab 8 Problems, Goodbye\n";
            return;
            break;
    }
}
void fix_file(ofstream & outfile)
{
    do
    {
        string file;
        cout << "Enter a new file name" << endl;
        
        cin >> file;
        
        outfile.open(file, ios::app);
    }
    while(outfile.fail);
}
