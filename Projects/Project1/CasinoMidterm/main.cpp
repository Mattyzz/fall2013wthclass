/*
 * Name: Matthew Dela Cruz
 * Student ID: 2479347
 * Date: 10/15/2013
 * Project 1
 * I certify this is my own work and code
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;
/*
 * Def : The diceroll function gets to random numbers between 1-6 and adds them
 * Ex. 4 and 6 = 10
 * 
 * Parameter: 2 Values representing to different dicerolls num1 and num2
 * 
 * Return: A number which is num1 and num2 added together
 */
int diceroll();

void blackjack();

/*
 * Def : cardPicker gets random numbers between 1-13
 * 
 * Parameter: a number representing the random number chosen
 * 
 * Return : The number that was chosen so that function that called it can use 
 * it to choose a card  
 */
int cardPicker();
void cardPoker(int & x);

/*
 * Def : This is the craps game 2 rounds exist
 *      Round1
 *      The diceroll function is called, if that number is a win or lose then
 *      the game ends and player either wins or lose x amount of chips/credits
 *      if it is not either of those numbers round2 starts
 * 
 *      Round2
 *      The 1st diceroll number is saved and another diceroll is called.
 *      If that diceroll is 7 you lose, if any else keep rolling until that
 *      number is equal to the first diceroll player wins bet.
 * 
 * Parameter : Referencing current chips
 * 
 * return : psuedo-returns for chip count
 * 
 */
void craps(int & x);
void Menu(int & x);
int creditadd(int & total, int gain);
int creditdeduct(int & total, int lose);

int main(int argc, char** argv) {
    srand(time(0));
    int credits = 100;
    Menu(credits);


    return 0;
}

int creditadd(int & total, int gain) //Referencing credits and adding to them
{
    cout << "I add credits" << endl;
}

int creditdeduct(int & total, int lose) //Referencing credits and deducting them
{
    total = total - lose;
}

int diceroll()  //Helper function for the craps function
{
    int num1 = rand() % 6 + 1;
    int num2 = rand() % 6 + 1;
    int total = num1 + num2;

    return total;
}

int cardPicker() 
{
   cout << "I am the card picker\n";             
}

void blackjack() 
{
    cout << "I am the blackjack game!\n";
}

void cardpoker(int & x) 
{
    cout << "Welcome to the 5 Card Poker Game!\n";
    cout << "You are playing for yourself\n";
    cout << "You currently have " << x << " chips\n";
    
    
}

void craps(int & x) 
{
    char end;
    do //The whole craps game in its entireity
    {
        cout << string(22, '\n'); //"Clearing" the console

        int bet;
        cout << "Welcome to the craps game!" << endl;
        cout << "You currently have " << x << " chips" << endl;
        cout << "Please enter your bet!" << endl;

        cin >> bet;

        cout << "Thank you, now rolling dice!" << endl;

        int dicesum = diceroll();
 
        //First round
        if (dicesum == 7 || dicesum == 11) {
            cout << "You won your bet!" << endl;
            creditadd(x, bet);
        } else if (dicesum == 2 || dicesum == 3 || dicesum == 12) {
            cout << "You lost D:" << endl;
            creditdeduct(x, bet);
        } 
        //Second round
        else 
        {
            
            cout << "You rolled " << dicesum << endl;
            int dicesum2;
            do {
                dicesum2 = diceroll();
                if (dicesum2 == 7) {
                    cout << "Diceroll is " << dicesum2 << endl;
                    cout << "You lost all bets" << endl;
                    creditdeduct(x, bet);

                } else if (dicesum2 == dicesum) {
                    cout << "Diceroll is " << dicesum2 << endl;
                    cout << "You won!" << endl;
                    creditadd(x ,bet);
                } else {
                    cout << "Diceroll is " << dicesum2 << endl;
                    cout << "Continuing dicerolls!" << endl;
                }


            }            while ((dicesum2 != dicesum) && dicesum2 != 7);
        }
        cout << "Do you want to play again Y/N" << endl;
        cin >> end;
    }   
    while (end != 'N' && end != 'n');
    cout << "Returning to Main menu" << endl;
    cout << string(22, '\n');
    return Menu(x);
}

void Menu(int & x) 
{
    

    int choice = 0;
    cout << "Welcome the casino please pick a game!\n";
    cout << "You currently have " << x << " credits\n";
    cout << "Enter 1 for Black Jack\n";
    cout << "Enter 2 for 5 Card Poker\n";
    cout << "Enter 3 for Craps\n";
    cout << "Enter anything else to leave the Casino" << endl;
    cin >> choice;

    switch (choice) 
    {
        case 1:
            blackjack();
            break;
        case 2:
            cardpoker(x);
            break;
        case 3:
            craps(x);
            break;
        default:
            cout << "Goodbye, please come back again soon... please D:\n";
            return;
    }
}