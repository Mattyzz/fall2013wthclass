/*
* Name: Matthew Dela Cruz
* Student ID: 2479347
* Date: 9/12/2013
* HW: 4
* Problem: 4
* I certify this is my own work and code
*/

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv)
{
    cout << "Welcome to 2-player Rock, Paper, Scissors!" << endl;
    cout << "When prompted please enter R, P, S!" << endl;
    char P1, P2;
    
    cout << "Player one, please enter your choice!: ";
    cin >> P1;
    
    cout << "Player two, please enter your choice!; ";
    cin >> P2;
    
    if(P1 == 'P'|| P1 == 'p')
    {
        if(P2 == 'R' || P2 == 'r' )
        {
            cout << "Paper covers rock!, P1 Wins!" << endl;
        }
        else if (P2 == 'S' || P2 == 's')
        {
            cout << "Scissors cuts paper, P2 Wins!" << endl;
        }
        else
            cout << "Its a tie, nobody won" << endl;

               
    }
    else if (P1 == 'R' || P1 == 'r')
    {
        if (P2 == 'P' || P2 == 'p')
        {
            cout << "Paper covers rock!, P2 Wins!" << endl;            
        }
        else if (P2 == 'S' || P2 == 's')
        {
            cout << "Rock breaks scissors, P1 Wins!" << endl;
        }
        else
            cout << "Its a tie, nobody won" << endl;
    }
    else if (P1 == 'S' || P1 == 's')
    {
        if(P2 == 'R' || P2 == 'r')
        {
            cout << "Rock breaks scissors, P2 Wins!" << endl;
        }
        else if (P2 == 'P' || P2 == 'p')
        {
            cout << "Scissors cuts paper, P1 Wins!" << endl;
        }
        else
            cout << "Its a tie, nobody won" << endl;
    }
    else
        cout << "Something was put in wrong, Please try again." << endl;
            

    return 0;
}

