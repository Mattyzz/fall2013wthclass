/*
* Name: Matthew Dela Cruz
* Student ID: 2479347
* Date: 9/12/2013
* HW: 4
* Problem: 3
* I certify this is my own work and code
*/

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv)
{
    int exercises;
    double average;
    double score = 0;
    double total = 0;
    int pointse;
    int totale;
    cout << "I calculate the overall grade within a classroom!\n";
    cout << "Please enter how many exercises there are: ";
    cin >> exercises;
    
    for(int i = 1; i <= exercises; i++)
    {
        cout << "Please enter points earned for exercise " << i << ": ";
        cin >> pointse;
        score = score + pointse;
        cout << "Please enter the maximum points possible for exercise " << i <<
                ": ";
        cin >> totale;
        total = total + totale;
        cout << endl;
                
    }
    cout << "Total points are: " << score << endl;
    cout << "Total poissible are: " << total << endl;
    cout << endl;
    average = (score / total) * 100; 
    cout << "The overall average is: " << average << "%" << endl;
    cout << "Thank you for using the average calculator!" << endl;
            
    
    

    return 0;
}

