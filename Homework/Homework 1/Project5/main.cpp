#include <iostream>
using namespace std;
int main()
{
    int number1, number2, product, sum; //variables
    cout << "Press enter after inputing a number.\n";
    cout << "Please enter number 1.\n";
    cin >> number1; 
    cout << "Now please enter number 2.\n";
    cin >> number2;
    sum = number1 + number2; //Addition
    cout << number1;
    cout << " + ";
    cout << number2;
    cout << " = ";
    cout << sum << endl; // outputs the sum and ends the line
    product = number1 * number2; // Multiplication
    cout << number1;
    cout << " * ";
    cout << number2;
    cout << " = ";
    cout << product << endl; // outputs the product and ends the line
    cout << "Restart to input new numbers.\n"; //Closing statement
    cout << "Goodbye!\n";
    return 0;
    
}
