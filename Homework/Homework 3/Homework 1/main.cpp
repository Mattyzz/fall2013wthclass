/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 24, 2013, 2:38 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv)
{
    string pnum;
    cout << "Enter a phone number with " <<
            "one dash in between the numbers" 
            << endl;
    cin >> pnum;
   int dashloc = pnum.find('-'); //need to use substr
   string fhalf = pnum.substr(0, dashloc);
   //cout << fhalf << endl;
   //string shalf = pnum.substr(dashloc+1, pnum.size() - dashloc - 1 );
   string shalf = pnum.substr(dashloc + 1);
   //cout << shalf << endl;
   cout << fhalf << shalf << endl;

    return 0;
}

