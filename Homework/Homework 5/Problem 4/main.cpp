/*
* Name: Matthew Dela Cruz
* Student ID: 2479347
* Date: 10/7/2013
* HW: 5
* Problem: 4
* I certify this is my own work and code
*/
#include <cstdlib>
#include <iostream>

using namespace std;

void Computers()
{
      srand(time(0));
       char end;
         do
         {
    cout << "Welcome to 2-player Rock, Paper, Scissors!, by bots" << endl;
    char P1, P2;
   // added + 1
    int x1 = rand() % 3 + 1; //random picker for P1

    int x2 = rand() % 3 + 1; // random picker for P2
   
    
    if (x1 == 1)      
    {
        P1 = 'S';
    }
    else if (x1 == 2)
    {
        P1 = 'R';
    }
    else if (x1 == 3)
    {
        P1 = 'P';
    }
    if (x2 == 1)
           {
        P2 = 'S';
    }
    else if (x2 == 2)
    {
        P2 = 'R';
    }
    else if (x2 == 3)
    {
        P2 = 'P';
    }
           
    if(P1 == 'P'|| P1 == 'p')
    {
        if(P2 == 'R' || P2 == 'r' )
        {
            cout << "Paper covers rock!, P1 Wins!" << endl;
        }
        else if (P2 == 'S' || P2 == 's')
        {
            cout << "Scissors cuts paper, P2 Wins!" << endl;
        }
        else
            cout << "Its a tie, nobody won" << endl;

               
    }
    else if (P1 == 'R' || P1 == 'r')
    {
        if (P2 == 'P' || P2 == 'p')
        {
            cout << "Paper covers rock!, P2 Wins!" << endl;            
        }
        else if (P2 == 'S' || P2 == 's')
        {
            cout << "Rock breaks scissors, P1 Wins!" << endl;
        }
        else
            cout << "Its a tie, nobody won" << endl;
    }
    else if (P1 == 'S' || P1 == 's')
    {
        if(P2 == 'R' || P2 == 'r')
        {
            cout << "Rock breaks scissors, P2 Wins!" << endl;
        }
        else if (P2 == 'P' || P2 == 'p')
        {
            cout << "Scissors cuts paper, P1 Wins!" << endl;
        }
        else
            cout << "Its a tie, nobody won" << endl;
    }
    else
        cout << "Something was put in wrong, Please try again." << endl;
    cout << "To play again please enter any character, if not type N: ";
    cin >> end;
    cout << endl;
    }  
    while(end != 'N');

    return;
}

void Humans()
 {
        char end;
        do
        {
        cout << "Welcome to 2-player Rock, Paper, Scissors!" << endl;
        cout << "When prompted please enter R, P, S!" << endl;
        char P1, P2;
    
        cout << "Player one, please enter your choice!: ";
        cin >> P1;
    
        cout << "Player two, please enter your choice!; ";
        cin >> P2;
    
        if(P1 == 'P'|| P1 == 'p')
                {
                if(P2 == 'R' || P2 == 'r' )
                 {
                cout << "Paper covers rock!, P1 Wins!" << endl;
                 }
                else if (P2 == 'S' || P2 == 's')
                 {
                cout << "Scissors cuts paper, P2 Wins!" << endl;
                }
                else
                cout << "Its a tie, nobody won" << endl;               
            }
        else if (P1 == 'R' || P1 == 'r')
                {
                 if (P2 == 'P' || P2 == 'p')
                {
                 cout << "Paper covers rock!, P2 Wins!" << endl;            
                }
                else if (P2 == 'S' || P2 == 's')
                {
                cout << "Rock breaks scissors, P1 Wins!" << endl;
                }
                else
                cout << "Its a tie, nobody won" << endl;
                }
        else if (P1 == 'S' || P1 == 's')
                {
                if(P2 == 'R' || P2 == 'r')
                {
                 cout << "Rock breaks scissors, P2 Wins!" << endl;
                 }
                else if (P2 == 'P' || P2 == 'p')
                {
                cout << "Scissors cuts paper, P1 Wins!" << endl;
                }
                else
                cout << "Its a tie, nobody won" << endl;
                }
         else
        cout << "Something was put in wrong, Please try again." << endl;
    cout << "To play again please enter any character, if not type N: ";
    cin >> end;
    cout << endl;
    }  
    while(end != 'N');

    return;
 }


/*
 * 
 */
int main(int argc, char** argv) 
{
    int choice;
    cout << "Enter a number to run a program" << endl;
    cout << " 1 for human rock paper scissors." << endl;
    cout << " 2 for computer rock paper scissors." << endl;
    cin >> choice;
    
    switch(choice)
    {
        case 1:
            Humans();
            break;
        case 2:
            Computers();
            break;
     }
    

    return 0;
}
