/*
* Name: Matthew Dela Cruz
* Student ID: 2479347
* Date: 9/12/2013
* HW: 2
* Problem: 3
* I certify this is my own work and code
*/


#include <iostream>
#include <iomanip>
#include <cstdlib>

using namespace std;

int main()
{
    int a, b, c; //Person1
    int d, e, f; //Person2
    int g, h, i; //Person3
    double average1, average2, average3;
    string name1, name2, name3;
    
    //Person1 and his values 
    cout << "Please enter first name: ";
        cin >> name1;
                cout << "Now enter his three scores: ";
                        cin >> a >> b >> c;
             
                
     //Person2 and his values          
    cout << "Please enter the second name: ";
        cin >> name2;
                cout << "Now enter his three scores: ";
                        cin >> d >> e >> f;   
     
      //Person3 and his values              
    cout << "Please enter the third name: ";
        cin >> name3;
                cout << "Now enter his three score: ";
                        cin >> g >> h >> i;
      
     
     //Mathematics
     average1 = static_cast<double>(a + d + g)/3;
     average2 = static_cast<double>(b + e + h)/3;  
     average3 = static_cast<double>(c + f + i)/3;
     
     
     //How everything is being printed out
     cout << left << setfill(' ')<< setw(10) << "Name" //Line 1
             << setw(10) << "Quiz 1"
             << setw(10) << "Quiz 2"
             << "Quiz 3\n";
     
     //Line2
     cout << left << setw(10) << "----"
             << setw(10) << "------"
             << setw(10) << "------"
             << "------\n";
     
     //Line3
     cout << left << setw(12) << name1
             << setw(10) << a
             << setw(10) << b
             << c << endl;
     //Line3
     cout << left <<  setw(12) << name2 
             << setw(10) << d
             << setw(10) << e
             << f << endl;
     //Line4
     cout << left << setw(12) << name3 
             << setw(10) << g
             << setw(10) << h
             << i << endl;
     
     //Spaces
     cout << "  \n";
     
     //Line5
     cout << left << setw(10) << "Average"
             << setw(10) << fixed << setprecision(2) << average1
             << setw(10) << average2
             << average3 << endl;
     return 0;
     
     
     
     
     
     
             
             
                
                
            
}
