/*
* Name: Matthew Dela Cruz
* Student ID: 2479347
* Date: 9/13/2013
* HW: 2
* Problem: 5
* I certify this is my own work and code
*/
#include <iostream>
#include <iomanip>

using namespace std;

int main()

{
    string name, name2, food, number, adj, animal, color;
    cout << "Welcome to Mad Lib! "
            << "Please enter what is asked as they come up" << endl;
    cout << "Please do not enter more than 10 characters for any input\n";
    cout << "A name: ";
    cin >> setw(11) >> name;
    cout << "Now another name: ";
    cin >> setw(11) >> name2;
    cout << "Enter your prefrence of food: ";
    cin >> setw(11) >> food;
    cout << "Now enter a number between 100-200: ";
    cin >> setw(11) >> number;
    cout << "Any adjective: ";
    cin >> setw(11) >> adj;
    cout << "An animal: ";
    cin >> setw(11) >> animal;
    cout << "Finally your color: ";
    cin >> setw(11) >> color;
    
    cout << "Dear " << name << "," << endl;
    cout << " " << endl;
    cout  << "I am sorry that I am unable "
            <<"to turn in my homework at this time. "
            << "First, I ate a rotten " << food << ','
            << " which me turn " << color << " and extermely ill."
            << " I came down with a fever of " << number << '.'
            << " Next, my " << adj << " pet " << animal
            << " must have smelled the remains of the " << food 
            << " on my homework because he ate it."
            << " I am currently rewriting my homework and hope you will accept"
            << " it late." << endl
            << " " << endl
            << "Sincerely,\n"
            << name2 << endl;
    return 0;
            
            
            
}